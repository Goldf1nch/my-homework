from tornado import web, ioloop
from Music_library import Index, Add_track, Delete_track


if __name__ == '__main__':
    Music_library = web.Application([
        (r'/', Index),
        (r'/add_track/', Add_track),
        (r'/delete_track/', Delete_track)
    ], debug=True)
    Music_library.listen(8000)
    ioloop.IOLoop.current().start()
