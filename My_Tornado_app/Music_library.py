from tornado import web
from sqlalchemy.orm import sessionmaker

from db import engine

from models import Music_library


class Index(web.RequestHandler):

    def get(self):
        session = sessionmaker(bind=engine)()
        table = session.query(Music_library).all()
        self.render('templates/index.html', table=table)


class Add_track(web.RequestHandler):
    def get(self):
        self.render('templates/add_track.html')

    def post(self):
        session = sessionmaker(bind=engine)()
        title = self.get_body_argument('title')
        artist = self.get_body_argument('artist')
        duration = self.get_body_argument('duration')
        if title:
            session.add(
                Music_library(artist=artist, title=title, duration=duration)
            )
            session.commit()
            self.redirect('/')
        else:
            self.render('templates/add_track.html')


class Delete_track(web.RequestHandler):

    def post(self):
        session = sessionmaker(bind=engine)()
        id = self.get_body_argument('id')
        session.query(Music_library).filter(Music_library.id == id).delete()
        session.commit()
        self.redirect('/')
