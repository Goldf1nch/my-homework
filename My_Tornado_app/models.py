from sqlalchemy import Column, Integer, String, REAL
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Music_library(Base):
    __tablename__ = "music_library"
    id = Column(Integer, primary_key=True)
    artist = Column(String, nullable=False)
    title = Column(String, nullable=False)
    duration = Column(REAL, nullable=False)
