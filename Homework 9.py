class HomeLibrary:
    def __init__(self):
        self.book_lst = []

    def book_add(self, data):
        data = data.split(', ')
        self.book_lst.append(data)
        return "Книга добавлена"

    def book_del(self, title):
        for elements in self.book_lst:
            if elements[1] == f'"{title}"':
                self.book_lst.remove(elements)
                return "Книга удалена"
        return "Не удалось найти книгу"

    def search_author(self, author):
        for elements in self.book_lst:
            if elements[0] == author:
                return f'{author} автор книги {elements[1]}'
        return "Не удалось найти книгу с заданным автором"

    def publ_sort(self):
        for i in range(len(self.book_lst) - 1):
            for j in range(len(self.book_lst) - i - 1):
                if self.book_lst[j][2] > self.book_lst[j + 1][2]:
                    self.book_lst[j], self.book_lst[j + 1] = self.book_lst[j + 1], self.book_lst[j]
        return self.book_lst


books = '''Михаил Булгаков, "Мастер и Мрагарита", 1967
Агата Кристи, "Десять Негритят", 1939
Лев Толстой, "Война и мир", 1865'''


lib = HomeLibrary()
for line in books.split('\n'):
    lib.book_add(line)

print(lib.book_del("12 стульев"))
print(lib.book_add('Илья Ильф, "12 стульев", 1928'))
print(lib.search_author("Михаил Булгаков"))
print(lib.book_del("Десять Негритят"))
print(lib.search_author("Агата Кристи"))
print(lib.publ_sort())