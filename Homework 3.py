'''1. Пользователь вводит число, если оно четное вывесть - “Even” если нет - “Odd” '''

# num = input()
# if num.isdigit():
#     if int(num) % 2 == 0:
#         print("Even")
#     else:
#         print("Odd")
# else:
#     print("Type error")

'''2. Пользователь вводит значения через запятую, если количество значений меньше 10, отсортировать их 
от большего к меньшему, если больше то от меньшего к большему.'''

# user_lst = [i for i in input().split(',')]
# for i in range(len(user_lst)):
#     if user_lst[i].isdigit():
#         user_lst[i] = int(user_lst[i])
#     else:
#         print('Error')
#         exit(0)
# if len(user_lst) < 10:
#     user_lst = sorted(user_lst)
#     user_lst.reverse()
#     print(user_lst)
# else:
#     user_lst = sorted(user_lst)
#     print(user_lst)

'''3. Пользователь вводит 2 числа, если первое больше второго, вывести их разность, если второе больше 
первого вывести их сумму, если числа равны, возвести первое в степень второго.'''

# num1, num2 = input(), input()
# if num1.isdigit() and num2.isdigit():
#     num1, num2 = int(num1), int(num2)
#     if num1 > num2:
#         print(num1 - num2)
#     elif num1 < num2:
#         print((num1 + num2))
#     else:
#         print(num1 ** num2)
# else:
#     print("Type error")

'''4. Пользователь вводит 3 числа, подставить и посчитать формулу: 2a - 8b / (a-b+c). Вывести результат.'''

# a, b, c = input("Введите a:"), input("Введите b:"), input("Введите c:")
# if a.isdigit() and b.isdigit() and c.isdigit():
#     if int(a) - int(b) + int(c) == 0:
#         print("Ошибка. Деление на 0")
#     else:
#         print((2 * int(a) - 8 * int(b)) / (int(a) - int(b) + int(c)))
# else:
#     print('Error')

'''5. Дан список из целых чисел длиной N подсчитать количество четных чисел в списке'''
#
# user_lst = [1, 2, 2, 3, 5, 6, 7, 8, 9, 10]
# num = 0
# for i in range(len(user_lst)):
#     if str(user_lst[i]).isdigit():
#         user_lst[i] = int(user_lst[i])
#     else:
#         print('Error')
#         exit(0)
# for elements in user_lst:
#     if elements % 2 == 0:
#         num += 1
# print(num)

'''6. Дан список целых чисел длиной N отсортировать список, не используя метода sort или sorted. 
(Пожалуйста не гуглите решение, постарайтесь сделать сами)'''
# Через 2 списка

# lst = [2, 4, 5, 7, 6, 21, 1, 3, 6, 7, 45, 9, 8, 75, 4, 32, 62, 13, 45, 67, 50, 9, 87, 654, 321, 1, 2345, 67, 2, 65, 3]
# lst1 = []
# for i in range(len(lst)):
#     if str(lst[i]).isdigit():
#         lst[i] = int(lst[i])
#     else:
#         print('Error')
#         exit(0)
# for i in range(len(lst)):
#     minimum = min(lst)
#     lst.remove(minimum)
#     lst1.append(minimum)
# print(lst1)

# Сортировка пузырьком

lst = [2, 4, 5, 7, 6, 21, 1, 3, 6, 7, 0, 9, 8, 75, 4, 32, 62, 13, 45, 67, 8, 9, 87, 654, 321, 1, 2345, 67, 2, 65, 3]
for i in range(len(lst)):
    if str(lst[i]).isdigit():
        lst[i] = int(lst[i])
    else:
        print('Error')
        exit(0)
for i in range(len(lst) - 1):
    for j in range(len(lst) - i - 1):
        if lst[j] > lst[j + 1]:
            lst[j], lst[j + 1] = lst[j + 1], lst[j]
print(lst)

'''7. Рассчитать последовательность фибоначчи, длину ряда задает пользователь'''

length = input()
fib = [0, 1]
if length.isdigit():
    if int(length) == 0:
        exit()
    elif int(length) == 1:
        print([0])
    else:
        for i in range(int((length)) - 2):
            fib.append(fib[i] + fib[i + 1])
        print(fib)
else:
    print("Type error")

'''8. Пользователь вводит целое число, определить простое ли оно.'''

# num = input()
# if num.isdigit():
#     num_sqrt = int(int(num) ** (1 / 2))
#     if int(num) % 2 == 0 and int(num) != 2:
#         print("Число не простое")
#     else:
#         for i in range(2, num_sqrt + 1):
#             if int(num) % i == 0:
#                 print("Число не простое")
#                 break
#         else:
#             print("Число простое")
# else:
#     print("Type error")