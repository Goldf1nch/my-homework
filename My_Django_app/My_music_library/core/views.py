from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.shortcuts import redirect
from django.http import HttpResponseRedirect

from .models import Track


class Index(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        tracks = Track.objects.all()
        return {
            "tracks": tracks
        }


class Add_track(CreateView):
    template_name = 'add_track.html'
    model = Track
    fields = ('artist', 'title', 'duration')

    def form_valid(self, form):
        form.save()
        return redirect('/')


class Delete_track(DeleteView):

    def delete(self, request, *args, **kwargs):
        track_id = request.POST['id']
        track = Track.objects.get(id=track_id)
        success_url = '/'
        track.delete()
        return HttpResponseRedirect(success_url)
