from django.db import models


class Track(models.Model):

    artist = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    duration = models.FloatField()
