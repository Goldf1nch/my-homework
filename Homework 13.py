import asyncio
import aiofiles
import time


async def reader(file_num):
    async with aiofiles.open(f'dir/in_{file_num}.dat', 'r') as in_file:
        data = await in_file.readlines()
        first = data[0]
        if first == "1\n":
            return await plus(data[1].split(' '))
        elif first == "2\n":
            return await multiply(data[1].split(' '))
        else:
            return await sum_of_squares(data[1].split(' '))


async def plus(data):
    file_sum = sum([float(x) for x in data])
    return file_sum


async def multiply(data):
    product = float(data[0])
    for elements in data[1:]:
        product *= float(elements)
    return product


async def sum_of_squares(data):
    sum_of_sq = sum([float(x) ** 2 for x in data])
    return sum_of_sq


async def main(counter):
    tasks = []
    for task in range(counter):
        tasks.append(asyncio.ensure_future(reader(task)))
    file_result = await asyncio.gather(*tasks)
    summa = sum(file_result)
    async with aiofiles.open('dir/out.dat', 'w') as out_file:
        await out_file.write(str(summa))


if __name__ == '__main__':
    files_count = input()
    t1 = time.time()
    if files_count.isdigit():
        event_loop = asyncio.get_event_loop()
        event_loop.run_until_complete(main(int(files_count)))
        print("Time", time.time() - t1)
