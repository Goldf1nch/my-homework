class Car:
    def __init__(self, name="BMW", color="white"):
        self.name = name
        self.speed = 0
        self.color = color
        self.state = None

    def __del__(self):
        print(f'The {self.name} stopped')

    def start(self):
        self.state = True
        print(f'The {self.name} started up')

    def drive(self, speed=40):
        self.speed = speed
        if self.state:
            print(f'The {self.name} is driving at a speed of {self.speed}')
        else:
            print(f'Start the {self.name} first')

    def painting(self, color="black"):
        self.color = color
        print(f'{self.name} is now {self.color}')


class Plane:

    def __init__(self, name="Boeing 737", people_on_board=0, altitude=0):
        self.name = name
        self.people = people_on_board
        self.altitude = altitude

    def board_passengers(self, num_of_people=0):
        self.altitude += num_of_people
        print(f'{self.altitude} passengers aboard a {self.name}')

    def takeoff(self):
        print(f'{self.name} takes off')

    def flight(self, altitude=1):
        self.altitude = altitude
        print(f'The {self.name} flies at an altitude of {self.altitude} kilometers')

    def landing(self):
        print(f'The {self.name} landed successfully')


class Ship:
    def __init__(self, name="Titanic", congestion=100, water_speed=10):
        self.name = name
        self.water_speed = water_speed
        self.congestion = congestion

    def swim(self, water_speed=10):
        self.water_speed = water_speed
        print(f'The {self.name} is sailing at speed {self.water_speed}')

    def embarkation(self, congestion):
        self.congestion = congestion
        print(f'{self.congestion} tons of cargo were loaded into the {self.name}')


class Amphibian(Car, Ship):

    def __init__(self, *args, water_speed=6, congestion=100, **kwargs):
        super(Amphibian, self).__init__(*args, **kwargs)
        self.water_speed = water_speed
        self.congestion = congestion

