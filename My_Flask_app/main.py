import sqlite3
from flask import Flask, render_template, request, redirect

main = Flask(__name__)


@main.route('/')
def index():
    with sqlite3.connect('tracks.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM tracks")
        tracks = cursor.fetchall()
    return render_template('index.html', tracks=tracks)


@main.route('/add_track/', methods=['GET', 'POST'])
def add_track():
    if request.method == 'POST':
        with sqlite3.connect('tracks.db') as connection:
            queries = f'INSERT INTO tracks (artist, title, duration) VALUES ("{request.form["artist"]}", ' \
                    f'"{request.form["title"]}", "{request.form["duration"]}")'
            cursor = connection.cursor()
            cursor.execute(queries)
            connection.commit()
            return redirect('/')
    return render_template('add_track.html')


@main.route('/delete_track/', methods=['GET', 'POST'])
def delete_track():
    if request.method == 'POST':
        with sqlite3.connect('tracks.db') as connection:
            cursor = connection.cursor()
            cursor.execute(f"DELETE FROM tracks WHERE id={request.form['title']}")
            connection.commit()
    return redirect('/')


if __name__ == '__main__':
    main.run(debug=True)
