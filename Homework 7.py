'''Написать декоратор который будет выводить на экран время выполнения функции.'''
from time import time, sleep


def lead_time(func):
    def wrapper(*args, **kwargs):
        time_before = time()
        func_exec = func(*args, **kwargs)
        time_after = time()
        print(f"Время выполнения функции: {time_after - time_before} секунд")
        return func_exec

    return wrapper


def some_func(time1):
    sleep(time1)
    return "Done"


print(1, lead_time(some_func(5)))

'''Сформировать убывающий массив из чисел, которые делятся на 3. Длину вводит пользователь'''

# def recursion(num=0, lst=[]):
#     if num == 0:
#         return lst
#     num -= 3
#     lst.append(num)
#     return recursion(num, lst)


# user_len = input("Введите длину последовательности:")
# if user_len.isdigit():
#     user_lst = recursion(int(user_len) * 3)
#     print(user_lst)

'''Написать декоратор skip который не выполняет функцию. Декоратор может принимать
 параметр который выдаст функция вместо реального результата'''

# def skip(func):
#     def decorator(*args, **kwargs):
#         return str(*args)
#
#     return decorator

'''Напишите генератор генерирующий последовательность треугольных чисел.'''

# def triangular_nums():
#     step = 0
#     while True:
#         num = (step * (step + 1)) / 2
#         yield num
#         step += 1
#
#
# a = triangular_nums()
# print(next(a))
