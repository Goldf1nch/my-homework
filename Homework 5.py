'''Написать программу перевода числа из арабских цифр в число из римских цифр.Написать программу обратного перевода можно ограничиться до 5000.'''

roman_numbers = {'I': 1,
                 'V': 5,
                 'X': 10,
                 'L': 50,
                 'C': 100,
                 'D': 500,
                 'M': 1000,
                 'v': 5000,
                 'm': 10000
                 }


def to_roman(num, unit, five, ten):
    num = int(num)
    if num < 4:
        return unit * num
    elif num == 4:
        return unit + five
    elif num == 5:
        return five
    elif 5 < num < 9:
        return five + unit * (num % 5)
    else:
        return unit + ten


def to_arabic(rom_num):
    num = 0
    previous_letter = None
    for letter in rom_num:
        if previous_letter and roman_numbers[previous_letter] < roman_numbers[letter]:
            num += (roman_numbers[letter] - 2 * roman_numbers[previous_letter])
        else:
            num += roman_numbers[letter]
        previous_letter = letter
    return num


user_arabic_num = input("Введите число арабскими цифрами: ")

if user_arabic_num.isdigit():
    if int(user_arabic_num) > 5000:
        print("Введите число меньше 5000")
        exit()
    for i in range(len(user_arabic_num), 0, -1):
        if i == 1:
            print(to_roman(user_arabic_num[-i], 'I', 'V', 'X'))
        elif i == 2:
            print(to_roman(user_arabic_num[-i], 'X', 'L', 'C'), end="")
        elif i == 3:
            print(to_roman(user_arabic_num[-i], 'C', 'D', 'M'), end="")
        elif i == 4:
            print(to_roman(user_arabic_num[-i], 'M', 'v', 'x'), end="")  # v = 5000, x = 10000
else:
    print("Некорректный ввод")
    exit()

user_roman_num = input("Введите число римскими цифрами: ")
try:
    print(to_arabic((user_roman_num)))
except:
    print("Не верно введено римское число")

'''Написать функцию no_numbers которая удаляет из файла все цифры. Функция должна принимать путь к файлу.'''

# def no_numbers(path):
#     with open(path, 'r') as file:
#         text = file.read()
#         with open(path, 'w') as file:
#             for sign in range(len(text)):
#                 if text[sign].isdigit():
#                     text.replace(text[sign], '')
#                 else:
#                     file.write(text[sign])
#
#
# path = input("Введите путь в файлу: ")
# try:
#     no_numbers(path)
# except:
#     print("Не верно введен путь в файлу")

'''Написать функцию реализующую дешифровку Цезаря, функция должна принимать путь к файлу и сдвиг.'''

# def decrypt_caesar(path, shift):
#     alp = 'abcdefghijklmnopqrstuvwxyz '
#     with open(path, 'r') as file:
#         text = file.read()
#         with open(path, 'w') as file:
#             for i in range(len(text)):
#                 file.write(alp[(alp.find(text[i]) - int(shift)) % len(alp)])
#
#
# user_path = input("Введите путь в файлу: ")
# user_shift = input('Введите сдвиг: ')
# if user_shift.isdigit():
#     try:
#         decrypt_caesar(user_path, user_shift)
#     except:
#         print("Не верно введен путь в файлу")
# else:
#     print("Type error")
