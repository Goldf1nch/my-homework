'''Дан файл с текстом, создать список из дат в тексте - даты в формате DD-MM-YYYY (02-03-1999)'''

from re import findall

with open('test.txt', 'r') as file:
    text = file.read()
    date_lst = findall(r'\d{2}-\d{2}-\d*', text)
print(date_lst)
new_date_lst = []
for elements in date_lst:
    if (31 >= int(elements[0] + elements[1]) != 0) and (12 >= int(elements[3] + elements[4]) != 0):
        new_date_lst.append(elements)
print(new_date_lst)
'''Пользователь вводит год в формате - YYYY (1999) Определить, является ли год високосным'''

from calendar import isleap

user_year = input()
try:
    if isleap(int(user_year)):
        print("Год високосный")
    else:
        print("Год не високосный")
except:
    print("TypeError")

'''Дан файл вывести сумму всех чисел в файле. Цифры могут быть не целыми и отрицательными'''

from re import findall

sum = 0
with open('test.txt', 'r') as file:
    text = file.read()
    lst = findall(r'-?\d+\.?\d*', text)
    for elements in lst:
        sum += float(elements)
    print(sum)
'''Написать генератор геометрической прогрессии'''

start, step, num = int(input("Введите первый член прогрессии: ")), int(input("Введите шаг: ")), \
                   int(input("Введите количесво элементов прогрессии: "))
lst = [(start * step ** pow) for pow in range(num)]
print(*lst)
'''Дан файл с текстом, удалить из файла все отрицательные числа, числа могут быть не целыми'''
from re import sub

with open('test.txt', 'r') as file:
    text = file.read()
    with open('test.txt', 'w') as file:
        file.write(sub(r'-+\d+\.?\d*', '', text))
'''Дан файл с текстом, вывесли список который содержит все имена, все фамилии и все географические 
названия. Имена, Фамилии и Географические названии в простом формате, без дифисов.'''

from re import findall

with open('test.txt', 'r', encoding='utf-8') as file:
    text = file.read()
    print(findall(r'[А-Я][а-я]+', text))
